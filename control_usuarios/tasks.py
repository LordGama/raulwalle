# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from celery.task.schedules import crontab
from celery.decorators import task
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from .models import Cliente
from .models import Servicio
from .models import Contratos
from .models import Factura
from dateutil import relativedelta
import calendar
from .models import Factura
from datetime import date, datetime, timedelta

logger = get_task_logger(__name__)


@task(name="sum_two_numbers")
def add(x, y):
    logger.info("sumresult" + str(x+y))
    return x + y

@periodic_task(run_every=(crontab(minute='*/3')), name="some_task", ignore_result=True)
def some_task():
    logger.info("doing something")
    return "Oida"

@periodic_task(
    name="prefill_pending_bills",
    run_every=(crontab(minute='*/3')),
    ignore_result=True
)
def prefill_pending_bills():

    logger.info("getting time")

    today = datetime.now()
    today_next_month = today + relativedelta.relativedelta(months=1)

    logger.info("searching missing invoices")

    contratos = Contratos.objects.distinct().exclude(factura__fecha_factura__range=(today,today_next_month))

    logger.info("filling missing invoices")

    for contrato in contratos:
        dia_sugerido = calendar.monthrange(today_next_month.year,today_next_month.month)[1]/calendar.monthrange(contrato.fecha_contrato.year,contrato.fecha_contrato.month)[1]*today_next_month.day
        fecha_sugerida = datetime(today.year,today_next_month.month,int(round(dia_sugerido)))
        nueva_factura = Factura(contrato = contrato, fecha_factura = fecha_sugerida, fecha_vencimiento = (fecha_sugerida + timedelta(days=3)))
        nueva_factura.save()

    logger.info("done prefilling")



    
