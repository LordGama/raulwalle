# Generated by Django 2.0.4 on 2018-09-06 01:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('control_usuarios', '0004_auto_20180905_2001'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='contrato',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='control_usuarios.Contratos'),
        ),
    ]
