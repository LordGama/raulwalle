# Generated by Django 2.0.4 on 2018-07-17 02:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('control_usuarios', '0002_auto_20180506_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cliente',
            name='clave',
            field=models.CharField(max_length=200),
        ),
    ]
