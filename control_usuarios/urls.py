from django.urls import path

#from . import views
from .views import servicio
from .views import views
from .views import contrato

urlpatterns = [
    path('', views.index, name='index'),
    path('login/',views.mlogin,name="login"),
    path('login/try',views.try_login,name="try_login"),
    path('logout/',views.mlogout,name="logout"),
    path('bill/pay', views.capture_bill_payment, name='pay_bill'),
    path('send_email/', views.send_email_to_client, name='send_email'),
    path('testr/',views.some_view,name="test_pdf"),
    path('contract/add',views.contract_add,name="contract_add"),
    path('contract/save',views.contract_save,name="contract_save"),
    path('services/',servicio.services,name="services"),
    path('services/save',servicio.service_save,name="service_save"),
    path('services/delete',servicio.service_delete,name="service_delete"),
    path('services/<int:service_id>/',servicio.service_edit, name='service_edit'),
    path('services/<int:service_id>/update',servicio.service_update, name='service_update'),
    path('clients/',views.clients,name="clients"),
    path('clients/<int:client_id>/',views.client,name="client"),
    path('clients/<int:client_id>/contracts/<int:contract_id>/',contrato.client_with_contract_details,name="client_with_contract"),
    path('clients/<int:client_id>/editar',views.client_edit,name="client_edit"),
    path('clients/anadir',views.client_add,name="client_add"),
    path('clients/<int:client_id>/add_contract',views.client_add_contract,name="client_add_contract"),
    path('clients/save',views.client_save,name="client_save"),
    path('clients/update/<int:client_id>',views.client_update,name="client_update"),
    path('clients/delete',views.client_delete,name="client_delete"),
]
