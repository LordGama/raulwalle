from django.views.generic.edit import UpdateView
from django import forms
from control_usuarios.models import Servicio

from django.shortcuts import get_object_or_404,render
from django.http import HttpResponse, HttpResponseRedirect, Http404

from django.urls import reverse
from django.contrib import messages

from django.contrib.auth.decorators import login_required

import re

@login_required(login_url='login/')
def services(request):
    servicios_lista = Servicio.objects.all().order_by('clave')
    context = {'servicios': servicios_lista}
    return render(request,'control_usuarios/services.html', context)

@login_required(login_url='login/')
def service_save(request):
    try:
        servicio_clave = request.POST["edit_text_clave_servicio"]
        servicio_descripcion = request.POST["edit_text_descripcion_servicio"]
        servicio_costo = request.POST["edit_text_costo_servicio"]
    except(KeyError):
        return render(request,'control_usuarios/services.html',{ 
            'message': "Falta Información"
            })
    else:

        servicio_tiene_usuarios = False
        if request.POST.get("edit_text_numero_usuarios_servicio", "0") == "1":
            servicio_tiene_usuarios = True


        nuevo_servicio = Servicio(clave=servicio_clave,nombre=servicio_descripcion,costo=servicio_costo,tiene_usuarios=servicio_tiene_usuarios)
        nuevo_servicio.save()

        return HttpResponseRedirect(reverse('services'))

@login_required(login_url='login/')
def service_delete(request):
    try:
        servicio_id = request.POST["edit_text_id_servicio"]
        servicio = get_object_or_404(Servicio,pk=servicio_id)
    except(KeyError):
        return render(request,'control_usuarios/services.html',{ 
            'message': "Falta Información"
            })
    except(Servicio.DoesNotExist):
        raise Http404("Servicio no existe")
    else:
        servicio.delete()
    return HttpResponseRedirect(reverse('services'))

@login_required(login_url='login/')
def service_edit(request,service_id):
    servicio = get_object_or_404(Servicio,pk=service_id)
    
    return render(request,'control_usuarios/service.html',{
        'servicio' : servicio
        })

@login_required(login_url='login/')
def service_update(request,service_id):
    servicio = get_object_or_404(Servicio,pk=service_id)

    try:
        servicio_clave = request.POST["edit_text_clave_servicio"]
        servicio_nombre = request.POST["edit_text_descripcion_servicio"]
        servicio_costo = request.POST["edit_text_costo_servicio"]
        # servicio_tiene_usuarios = request.POST["edit_text_numero_usuarios_servicio"]
    except(KeyError):
        messages.error(request,"faltan datos!")
        return HttpResponseRedirect(reverse('service_edit',kwargs={'service_id': service_id}))
    else:

        servicio_tiene_usuarios = request.POST.get("edit_text_numero_usuarios_servicio",False)

        servicio.clave = servicio_clave
        servicio.nombre = servicio_nombre
        servicio.costo = servicio_costo
        servicio.tiene_usuarios = servicio_tiene_usuarios

        servicio.save()

        return HttpResponseRedirect(reverse('services'))


