from django.views.generic.edit import UpdateView
from django import forms
from control_usuarios.models import Contratos

from django.shortcuts import get_object_or_404,render
from django.http import HttpResponse, HttpResponseRedirect, Http404

from django.contrib.auth.decorators import login_required

class ContratoUpdate(UpdateView):
    model = Contratos
    fields = ['clave','nombre','costo','tiene_usuarios']

@login_required(login_url='login/')
def client_with_contract_details(request,client_id,contract_id):
    contrato = get_object_or_404(Contratos,pk=contract_id)
    
    return render(request,'control_usuarios/contract_details.html',{
        'contrato' : contrato
        })

    