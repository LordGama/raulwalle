from django.shortcuts import get_object_or_404,render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from control_usuarios.models import Cliente
from control_usuarios.models import Servicio
from control_usuarios.models import Contratos
from control_usuarios.models import Factura
from control_usuarios.models import Usuario
from django.db.models import Q
from control_usuarios.utils import ClientMode
from control_usuarios.utils import render_to_pdf
from control_usuarios.utils import DecimalEncoder
from django.urls import reverse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from datetime import date, datetime, timedelta
from dateutil import relativedelta
import calendar
from calendar import monthrange
import json
from django.db.models import Count
from django.db.models import Q
from django_pivot.pivot import pivot
from django_pivot.histogram import histogram
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist

import reportlab
import io
from django.http import FileResponse
from reportlab.pdfgen import canvas
from django.conf import settings

from django.core.mail import send_mail
from django.core.mail import EmailMessage

import control_usuarios.tasks

# from .tasks import add
# from control_usuarios.tasks import prefill_pending_bills
# from control_usuarios.tasks import some_task

from django.contrib import messages

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

def some_view(request):

    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    # response['Content-Disposition'] = 'attachment;filename="somefilename.pdf"'
    response['Content-Disposition'] = 'filename="somefilename.pdf"'

    print("entro some v")
    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()

    # Create the PDF object, using the buffer as its "file."
    #p = canvas.Canvas(buffer)

    #p = canvas.Canvas(response)
    p = canvas.Canvas(settings.MEDIA_ROOT + 'pdfs/file_name.pdf')

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 100, "Hello world.")

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()


    data = {
            'today': datetime.now(), 
            'amount': 39.99,
            'customer_name': 'Cooper Mann',
            'order_id': 1233434,
        }
    pdf = render_to_pdf('pdf/invoice.html', data)

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    #return FileResponse(buffer, as_attachment=True, filename='hello.pdf')
    return pdf

def mlogin(request):
    return render(request,"control_usuarios/login.html",{})

def mlogout(request):
    logout(request)
    return HttpResponseRedirect(reverse('login'))

def try_login(request):
    username = request.POST['edit_text_usuario']
    password = request.POST['edit_text_contrasena']
    user = authenticate(request,username=username,password=password)
    if user is not None:
        login(request,user)
        return HttpResponseRedirect(reverse('index'))
    else:
        messages.error(request,"Login Invalido")
        return HttpResponseRedirect(reverse('login'))

# Create your views here.
@login_required(login_url='login/')
def index(request):

    fecha_limite = datetime.now() + timedelta(days=7)

    if 'facturas_hasta' in request.GET:
        fecha_hasta = datetime.strptime(request.GET['facturas_hasta'],"%Y-%m-%d")
    else:
        fecha_hasta = datetime.now() + timedelta(days=7)

    if 'facturas_desde' in request.GET:
        fecha_desde = datetime.strptime(request.GET['facturas_desde'],"%Y-%m-%d")
    else:
        fecha_desde = datetime.now() - timedelta(days=7)
    

    cliente_search_string = request.GET.get("cliente","")

    # fecha_desde = datetime.now()
    # fecha_hasta = fecha_desde + relativedelta.relativedelta(months=1)

    #clientes_lista = Cliente.objects.filter(Q(nombre__icontains=request.GET['search']) | Q(clave__icontains=request.GET['search']) ).order_by('nombre')

    

    #contratos = Contratos.objects.distinct().exclude(factura__fecha_factura__range=(fecha_desde,fecha_hasta))
    #contratos = Contratos.objects.filter(factura__fecha_factura__range=(fecha_desde,fecha_hasta))
    if 'cliente' in request.GET:
        facturas = Factura.objects.filter( (Q(contrato__cliente__nombre__icontains=request.GET['cliente']) | Q(contrato__cliente__clave__icontains=request.GET['cliente'])) & Q(fecha_factura__range=(fecha_desde,fecha_hasta)) )
    else:
        facturas = Factura.objects.filter(fecha_factura__range=(fecha_desde,fecha_hasta))

    # add.delay(5,7)

    context = {
        'bills' : facturas,
        'cliente_string': cliente_search_string,
        'fechaDesde': fecha_desde.strftime("%Y-%m-%d"),
        'fechaHasta': fecha_hasta.strftime("%Y-%m-%d"),
        'fechaLimite': fecha_limite.strftime("%Y-%m-%d"),

    }

    return render(request, 'control_usuarios/index.html', context)

def monthdelta(d1, d2):
    delta = 0
    while True:
        mdays = monthrange(d1.year, d1.month)[1]
        d1 += timedelta(days=mdays)
        if d1 <= d2:
            delta += 1
        else:
            break
    return delta

@login_required(login_url='login/')
def send_email_to_client(request):
    next = request.POST.get('next', '/')

    try:
        email_to = request.POST["edit_text_email_to"]
        email_asunto = request.POST["edit_text_asunto"]
        email_body = request.POST["edit_text_body"]
    except(KeyError):
        messages.error(request,"faltan datos!")
        return HttpResponseRedirect(next)
    else:
        email = EmailMessage(subject=email_asunto,body=email_body,to=[email_to])

        files = request.FILES.getlist("edit_text_email_files")

        for f in files:
            email.attach(f.name, f.read(), f.content_type)
        #email.attach('invoicex.pdf', pdf.getvalue() , 'application/pdf')
        email.send()
        messages.success(request,"E-mail enviado exitosamente.")
        return HttpResponseRedirect(next)







@login_required(login_url='login/')
def capture_bill_payment(request):
    try:
        factura_id = request.POST['edit_text_factura']
        factura = get_object_or_404(Factura,pk=factura_id)
    except(KeyError):
        return render(request,'control_usuarios/index.html',{'mensaje' : 'falta información'})
    except(Factura.DoesNotExist):
        return render(request,'control_usuarios/index.html',{'mensaje' : 'falta información'})
    else:
        factura.pagado = True
        factura.save()

        cliente = factura.contrato.cliente
        servicio = factura.contrato.servicio

        recibo_data = {
            'cliente': cliente, 
            'servicio': servicio,
        }
        pdf = render_to_pdf('pdf/invoice.html', recibo_data)

        email = EmailMessage(subject='Pago exitoso',body='Hola '+ cliente.nombre +', ¡Muchas gracias! Tu pago por $'+ str(servicio.costo) +' ha sido recibido con éxito.',to=[cliente.email])
        email.attach('invoicex.pdf', pdf.getvalue() , 'application/pdf')
        email.send()
    return HttpResponseRedirect(reverse('index'))
    
        
@login_required(login_url='login/')
def contract_add(request):
    servicios_lista = Servicio.objects.all()
    json_servicios = []

    for servicio in servicios_lista:
        leer_servicio = {
                    'value': servicio.id,
                    'clave': servicio.clave,
                    'name': servicio.nombre,
                    'hasUsers': servicio.tiene_usuarios,
                    'costo': float(servicio.costo)
                }
        json_servicios.append(leer_servicio)

    json_servicios = json.dumps(json_servicios)
    # print(json_servicios)

    context = {
        'servicios': servicios_lista,
        'json_servicios': json_servicios
        }
    return render(request,'control_usuarios/deals.html',context)

@login_required(login_url='login/')
def contract_save(request):
    try:
        #cliente nuevo
        cliente_nombre  = request.POST['edit_text_cliente_nombre']
        cliente_fecha_nacimiento  = request.POST['edit_text_cliente_fecha_nacimiento']
        cliente_sexo = request.POST['radio_button_cliente_sexo']
        cliente_email = request.POST['edit_text_cliente_email']
        cliente_telefono  = request.POST['edit_text_cliente_telefono']
        cliente_direccion = request.POST['edit_text_cliente_direccion']

        #Servicios Seleccionados
        service_ids = request.POST.getlist("edit_text_cliente_contrato_servicio[]")
        service_users = request.POST.getlist("edit_text_cliente_contrato_usuario[]")
        service_passwords = request.POST.getlist("edit_text_cliente_contrato_pass[]")
        service_addresses = request.POST.getlist("edit_text_cliente_contrato_direccion[]")
        
    except(KeyError):
        return render(request,'control_usuarios/deals.html',{'error_message': 'Falta Información'})
    else:
        if(cliente_nombre == '' or cliente_telefono == '' or cliente_direccion == ''):
            return render(request,'control_usuarios/clients_details.html',{
            'message': "Falta Información",
            'cliente':cliente,
            'mode': ClientMode.EDIT,
            'MODE_EDIT': ClientMode.EDIT,
            'MODE_CREATE': ClientMode.CREATE,
            'MODE_VIEW': ClientMode.VIEW
            })

        fecha_actual = datetime.now().strftime("%Y-%m-%d")
        try:
            ultimo_cliente = Cliente.objects.latest('id')
            nuevo_cliente = Cliente(clave=""+cliente_nombre[:2].upper()+"28"+str(ultimo_cliente.id+1)+"",nombre=cliente_nombre,direccion=cliente_direccion,telefono=cliente_telefono,fecha_nacimiento=cliente_fecha_nacimiento,sexo=cliente_sexo,email=cliente_email)
        except(Cliente.DoesNotExist):
            nuevo_cliente = Cliente(clave=""+cliente_nombre[:2].upper()+"28"+str(1)+"",nombre=cliente_nombre,direccion=cliente_direccion,telefono=cliente_telefono,fecha_nacimiento=cliente_fecha_nacimiento,sexo=cliente_sexo,email=cliente_email)
        nuevo_cliente.save()

        for s,a in zip(service_ids,service_addresses):

            servicio = get_object_or_404(Servicio, pk=s)
            cliente_servicio = Contratos(cliente = nuevo_cliente,servicio=servicio,fecha_contrato=fecha_actual,direccion_servicio=a)
            cliente_servicio.save()

            if(servicio.tiene_usuarios):
                usuario = Usuario(contrato=cliente_servicio,usuario=service_user,clave=service_password)
                usuario.save()


        # for service_id in service_ids:
        #     cliente_servicio = Contratos(cliente = ultimo_cliente,servicio__pk=service_id,fecha_contrato=fecha_actual)
        #     cliente_servicio.save()

        return HttpResponseRedirect(reverse('index'))

@login_required(login_url='login/')
def clients(request):
    if 'search' in request.GET:
        clientes_lista = Cliente.objects.filter(Q(nombre__icontains=request.GET['search']) | Q(clave__icontains=request.GET['search']) ).order_by('nombre')
    else:
        clientes_lista = Cliente.objects.all().order_by('nombre')
    #clientes_lista = Cliente.objects.all()
    paginator = Paginator(clientes_lista, 7)
    page = request.GET.get('page')
    clientes = paginator.get_page(page)
    context = {'clientes': clientes}
    return render(request, 'control_usuarios/clients.html', context)

@login_required(login_url='login/')
def client(request, client_id):
    cliente = get_object_or_404(Cliente, pk=client_id)

    #Servicios

    servicios_lista = Servicio.objects.all()
    json_servicios = []

    for servicio in servicios_lista:
        leer_servicio = {
                    'value': servicio.id,
                    'clave': servicio.clave,
                    'name': servicio.nombre,
                    'hasUsers': servicio.tiene_usuarios,
                    'costo': float(servicio.costo)
                }
        json_servicios.append(leer_servicio)

    json_servicios = json.dumps(json_servicios,cls=DecimalEncoder)

    return render(request,'control_usuarios/clients_details.html',{
        'json_servicios': json_servicios,
        'cliente':cliente,
        'mode': ClientMode.VIEW,
        'MODE_EDIT': ClientMode.EDIT,
        'MODE_CREATE': ClientMode.CREATE,
        'MODE_VIEW': ClientMode.VIEW
         })

@login_required(login_url='login/')
def client_edit(request, client_id):
    cliente = get_object_or_404(Cliente, pk=client_id)
    return render(request,'control_usuarios/clients_details.html',{ 
        'json_servicios': '{}',
        'cliente':cliente,
        'mode': ClientMode.EDIT,
        'MODE_EDIT': ClientMode.EDIT,
        'MODE_CREATE': ClientMode.CREATE,
        'MODE_VIEW': ClientMode.VIEW
        })

@login_required(login_url='login/')
def client_add(request):
    return render(request,'control_usuarios/clients_details.html',{
        'json_servicios': '{}',
        'mode': ClientMode.CREATE,
        'MODE_EDIT': ClientMode.EDIT,
        'MODE_CREATE': ClientMode.CREATE
         })

@login_required(login_url='login/')
def client_add_contract(request,client_id):
    try:
        cliente = get_object_or_404(Cliente,pk=client_id)

        #Servicios Seleccionado
        service_id = request.POST.get("edit_text_cliente_contrato_servicio",0)
        service_user = request.POST.get("edit_text_cliente_contrato_usuario","")
        service_password = request.POST.get("edit_text_cliente_contrato_pass","")
        service_address = request.POST.get("edit_text_cliente_contrato_direccion","")
        servicio = get_object_or_404(Servicio,pk=service_id)
    # except(KeyError):
    #     messages.error(request,"faltan datos!")
    #     return HttpResponseRedirect(reverse('client',kwargs={'client_id': client_id}))
    except(Cliente.DoesNotExist):
        raise Http404("El cliente no existe")
    except(Servicio.DoesNotExist):
        raise Http404("El servicio no existe")
    else:
        #fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")

        cliente_servicio = Contratos(cliente = cliente,servicio=servicio,fecha_contrato=datetime.now(),direccion_servicio=service_address)
        cliente_servicio.save()

        if(servicio.tiene_usuarios):
            usuario = Usuario(contrato=cliente_servicio,usuario=service_user,clave=service_password)
            usuario.save()

        return HttpResponseRedirect(reverse('client',kwargs={'client_id': client_id}))

@login_required(login_url='login/')
def client_delete(request):
    try:
        cliente_id = request.POST["edit_text_id_cliente"]
        cliente = get_object_or_404(Cliente,pk=cliente_id)
    except(KeyError):
        return render(request,'control_usuarios/clients.html',{ 
            'message': "Falta Información"
            })
    except(Cliente.DoesNotExist):
        raise Http404("Servicio no existe")
    else:
        cliente.delete()
    return HttpResponseRedirect(reverse('clients'))

@login_required(login_url='login/')
def client_save(request):
    try:
        cliente_nombre  = request.POST['edit_text_cliente_nombre']
        cliente_fecha_nacimiento  = request.POST['edit_text_cliente_fecha_nacimiento']
        cliente_sexo = request.POST['radio_button_cliente_sexo']
        cliente_email = request.POST['edit_text_cliente_email']
        cliente_telefono  = request.POST['edit_text_cliente_telefono']
        cliente_direccion = request.POST['edit_text_cliente_direccion']
    except(KeyError):
        return render(request,'control_usuarios/clients_details.html',{ 
            'mode': ClientMode.CREATE,
            'MODE_EDIT': ClientMode.EDIT,
            'MODE_CREATE': ClientMode.CREATE,
            'MODE_VIEW': ClientMode.VIEW,
            'message': "Falta Información"
            })
    else:
        if(cliente_nombre == '' or cliente_telefono == '' or cliente_direccion == ''):
            return render(request,'control_usuarios/clients_details.html',{
            'message': "Falta Información",
            'cliente':cliente,
            'mode': ClientMode.EDIT,
            'MODE_EDIT': ClientMode.EDIT,
            'MODE_CREATE': ClientMode.CREATE,
            'MODE_VIEW': ClientMode.VIEW
            })

        try:
            ultimo_cliente = Cliente.objects.latest('id')
            nuevo_cliente = Cliente(clave=""+cliente_nombre[:2].upper()+"28"+str(ultimo_cliente.id+1)+"",nombre=cliente_nombre,direccion=cliente_direccion,telefono=cliente_telefono,fecha_nacimiento=cliente_fecha_nacimiento,sexo=cliente_sexo,email=cliente_email)
            nuevo_cliente.save()

            return HttpResponseRedirect(reverse('clients'))
        except(Cliente.DoesNotExist):
            nuevo_cliente = Cliente(clave=""+cliente_nombre[:2].upper()+"28"+str(1)+"",nombre=cliente_nombre,direccion=cliente_direccion,telefono=cliente_telefono,fecha_nacimiento=cliente_fecha_nacimiento,sexo=cliente_sexo,email=cliente_email)
            nuevo_cliente.save()

            return HttpResponseRedirect(reverse('clients'))

@login_required(login_url='login/')
def client_update(request, client_id):
    try:
        cliente_nombre  = request.POST['edit_text_cliente_nombre']
        cliente_fecha_nacimiento  = request.POST['edit_text_cliente_fecha_nacimiento']
        cliente_sexo = request.POST['radio_button_cliente_sexo']
        cliente_email = request.POST['edit_text_cliente_email']
        cliente_telefono  = request.POST['edit_text_cliente_telefono']
        cliente_direccion = request.POST['edit_text_cliente_direccion']
        cliente = get_object_or_404(Cliente, pk=client_id)
    except(KeyError):
        return render(request,'control_usuarios/clients_details.html',{ 'message': "Falta Información"})
    except(Cliente.DoesNotExist):
        return render(request,'control_usuarios/clients_details.html',{ 'message': "El cliente no existe"})
    else:
        if(cliente_nombre == '' or cliente_telefono == '' or cliente_direccion == ''):
            return render(request,'control_usuarios/clients_details.html',{
            'message': "Falta Información",
            'cliente':cliente,
            'mode': ClientMode.EDIT,
            'MODE_EDIT': ClientMode.EDIT,
            'MODE_CREATE': ClientMode.CREATE,
            'MODE_VIEW': ClientMode.VIEW
            })

        cliente.nombre = cliente_nombre
        cliente.direccion = cliente_direccion
        cliente.telefono = cliente_telefono
        cliente.fecha_nacimiento = cliente_fecha_nacimiento
        cliente.sexo = cliente_sexo
        cliente.email = cliente_email
        cliente.save()

        return HttpResponseRedirect(reverse('clients'))