from django.db import models
from django.urls import reverse

# Create your models here.
class Cliente(models.Model):
    clave = models.CharField(max_length=200)
    nombre = models.CharField(max_length=200)
    fecha_nacimiento = models.CharField(max_length=10,default="14/10/2018")
    sexo = models.CharField(max_length=1,default="O")
    email = models.CharField(max_length=50,default="")
    direccion = models.CharField(max_length=200)
    telefono = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

class Servicio(models.Model):
    nombre = models.CharField(max_length=200)
    clave = models.CharField(max_length=5)
    costo = models.DecimalField(default=0.00, max_digits=6, decimal_places=2)
    tiene_usuarios = models.BooleanField(default=False)
    clientes = models.ManyToManyField(Cliente, through='Contratos')

    def __str__(self):
        return self.nombre

    # def get_absolute_url(self):
    #     return reverse('service-detail',kwargs={'pk':self.pk})
    
    def get_absolute_url(self):
        return reverse('services')

class Contratos(models.Model):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    fecha_contrato = models.DateField()
    direccion_servicio = models.CharField(max_length=200,default="")

    def __str__(self):
        return ""+ str(self.id) +"_"+ self.cliente.nombre +"__"+self.servicio.nombre+""
    #agregar al modelo fecha vencimiento

class Factura(models.Model):
    contrato = models.ForeignKey(Contratos, on_delete=models.CASCADE)
    fecha_factura = models.DateField()
    fecha_vencimiento = models.DateField()
    pagado = models.BooleanField(default=False)

    def __str__(self):
        return self.contrato.cliente.clave+ "_" + self.contrato.servicio.nombre+"--" + self.fecha_factura.strftime("%Y-%m-%d")

class Usuario(models.Model):
    contrato = models.ForeignKey(Contratos, on_delete=models.CASCADE,default=1)
    usuario = models.CharField(max_length=200)
    clave = models.CharField(max_length=200)

class Tipo_Usuario(models.Model):
    descripcion = models.CharField(max_length=200)
    
