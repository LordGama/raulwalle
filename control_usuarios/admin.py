from django.contrib import admin

# Register your models here.

from .models import Cliente
from .models import Servicio
from .models import Contratos
from .models import Factura
from .models import Usuario

admin.site.register(Cliente)
admin.site.register(Servicio)
admin.site.register(Contratos)
admin.site.register(Factura)
admin.site.register(Usuario)